﻿using UnityEngine;
using System.Collections;

public class pickup : MonoBehaviour
{
    public GameObject effect;
    public AudioClip sound;
    static public int scoreCount = 1;

    private ParticleSystem ps;
    private soundManager m_soundManager;

    void Awake()
    {
        ps = effect.GetComponent<ParticleSystem>();
        m_soundManager = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<soundManager>();
    }

    void Start()
    {

    }

    void Update()
    {

    }

    IEnumerator DoShowEffect()
    {
        if (effect != null)
        {
            GameObject ob = Instantiate(effect, transform.position, Quaternion.identity, gameObject.transform.parent) as GameObject;
            Destroy(ob, ps.duration);
        }
        yield return null;
    }

    IEnumerator DoPlaySound()
    {
        if (m_soundManager != null)
            m_soundManager.PlaySound(sound);
        yield return null;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (gameObject.CompareTag("PickUp"))
            {
                other.gameObject.GetComponent<player>().AddScore(1);
            }


            StartCoroutine(DoPlaySound());
            StartCoroutine(DoShowEffect());
            gameObject.SetActive(false);
        }
    }

}
