﻿using UnityEngine;
using System.Collections;

public class hover : MonoBehaviour {

    public float m_speedX = 0f;
    public float m_speedY = 0f;
    public float m_speedZ = 0f;
    public float m_hoverX = 0f;
    public float m_hoverY = 0f;
    public float m_hoverZ = 0f;

    float startX;
    float startZ;
    float startY;

    void Start () {
        startX = transform.localPosition.x;
        startY = transform.localPosition.y;
        startZ = transform.localPosition.z;
    }

    void Update () {
        Vector3 position = transform.localPosition;
//        position = new Vector3(position.x, startY + Mathf.Sin(Time.time * m_speed) * m_hover, position.z);
        position = new Vector3(startX + Mathf.Sin(Time.time * m_speedX) * m_hoverX, startY + Mathf.Sin(Time.time * m_speedY) * m_hoverY, startZ + Mathf.Sin(Time.time * m_speedZ) * m_hoverZ);
        transform.localPosition = position;
    }
}
