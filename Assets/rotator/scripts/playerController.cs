﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class playerController : MonoBehaviour
{

    public float rotateSpeed;

    private Vector3 pivot;
    private Bounds m_bounds;

    static player m_player;

    static private bool leftTouched = false;
    static bool rightTouched = false;

    void Awake()
    {

        m_player = GameObject.FindGameObjectWithTag("Player").GetComponent<player>();

        BoxCollider boxCollider = gameObject.AddComponent<BoxCollider>();
        boxCollider.isTrigger = true;
        utils.FitColliderToChildren(gameObject);
        pivot = boxCollider.bounds.center;
        m_bounds = boxCollider.bounds;
    }

    void Start()
    {
        leftTouched = false;
        rightTouched = false;
}

    void FixedUpdate()
    {
        if (m_player.IsActive())
        {
            float moveHorizontal = Input.GetAxis("Horizontal");
            transform.RotateAround(pivot, -Vector3.forward, rotateSpeed * moveHorizontal);

            if (leftTouched)
                RotateLeft();

            if (rightTouched)
                RotateRight();
        }
    }


    public void LeftPointerDown()
    {
        if (!leftTouched)
        {
            leftTouched = true;
        }
    }

    public void LeftPointerUp()
    {
        leftTouched = false;
    }

    public void RightPointerUp()
    {
        rightTouched = false;
    }

    public void RightPointerDown()
    {
        if (!rightTouched)
        {
            rightTouched = true;
        }
    }

    public void RotateLeft()
    {
        float moveHorizontal = 1;
        transform.RotateAround(pivot, Vector3.forward, rotateSpeed * moveHorizontal);
    }

    public void RotateRight()
    {
        float moveHorizontal = -1;
        transform.RotateAround(pivot, Vector3.forward, rotateSpeed * moveHorizontal);
    }

    public Bounds GetBounds()
    {
        return m_bounds;
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

    }

}
