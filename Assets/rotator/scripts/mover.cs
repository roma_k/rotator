﻿using UnityEngine;
using System.Collections;

public class mover : MonoBehaviour
{

    public float speedX = 0;
    public float speedY = 0;
    public float speedZ = 0;

    public float moveX = 0;

    private Vector3 startPos;

    void Awake()
    {
        startPos = transform.position;

    }

    void Start()
    {
        Vector3 end = transform.position;
        end.x -= 5f;
        StartCoroutine(MoveOverSeconds(gameObject, end, 3f));
    }

    void Update()
    {
        //transform.Translate	
    }

    public IEnumerator MoveOverSeconds(GameObject objectToMove, Vector3 end, float seconds)
    {
        float elapsedTime = 0;
        Vector3 startingPos = objectToMove.transform.position;
        while (elapsedTime < seconds)
        {
            transform.position = Vector3.Lerp(startingPos, end, (elapsedTime / seconds));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        //StartCoroutine(MoveOverSeconds(gameObject, startPos, seconds)));
        //MoveOverSeconds(objectToMove, end, seconds);
    }


    public IEnumerator MoveOverSpeed(GameObject objectToMove, Vector3 end, float speed)
    {
        // speed should be 1 unit per second
        while (objectToMove.transform.position != end)
        {
            objectToMove.transform.position = Vector3.MoveTowards(objectToMove.transform.position, end, speed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }
}
