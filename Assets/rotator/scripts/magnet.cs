﻿using UnityEngine;
using System.Collections;

public class magnet : MonoBehaviour
{

    public float radius = 5f;
    public float lifeTime = 5f;

    void Start()
    {

    }

    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            player pl = other.gameObject.GetComponent<player>();
            pl.ActivateMagnet(lifeTime, radius);
        }
    }

}
