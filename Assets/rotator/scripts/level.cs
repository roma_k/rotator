﻿using UnityEngine;
using System.Collections;

public class level : MonoBehaviour {

    public float cameraDistance = 10f;
    public float cameraHeight = 1f;
    public float cameraHeightDamping = 2f;

    public int curLevel;


    void Start () {
        SmoothFollow sf = Camera.main.GetComponent<SmoothFollow>();
        sf.distance = cameraDistance;
        sf.height = cameraHeight;
        sf.heightDamping = cameraHeightDamping;

        gameManager.instance.SetCurLevel(curLevel);
    }

    void Update () {
	
	}
}
