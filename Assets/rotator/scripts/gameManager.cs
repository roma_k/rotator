﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class gameManager : MonoBehaviour
{
    public int totalLevels;
    public int curLevel;


    int bestScore;
    int lastTry;

    GameObject levelBounds;
    GameObject level;

    private static gameManager _instance;

    public static gameManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<gameManager>();
            }
            return _instance;
        }
    }

    void Awake()
    {
        if (_instance != null)
        {
            if (_instance.gameObject != gameObject)
            {
                Destroy(gameObject);
                return;
            }
        }
        _instance = GameObject.FindObjectOfType<gameManager>();

        curLevel = PlayerPrefs.GetInt("curLevel");

        bestScore = PlayerPrefs.GetInt("bestScore");
        lastTry = PlayerPrefs.GetInt("lastTry");

        levelBounds = GameObject.FindGameObjectWithTag("LevelBounds");
        level = GameObject.FindGameObjectWithTag("Level");
    }

    void Start()
    {
        Bounds bounds = level.GetComponent<BoxCollider>().bounds;
        BoxCollider bc = levelBounds.GetComponent<BoxCollider>();
        bc.center = bounds.center;
        Vector3 size = bounds.size;
        size.y += 10f;
        size.z += 1f;
        size.x += 5f;
        bc.size = size;
        levelBounds.transform.position = level.transform.position;
    }

    void Update()
    {

    }

    public void SetCurLevel(int l)
    {
        curLevel = l;
        PlayerPrefs.SetInt("curLevel", curLevel);
    }

    public IEnumerator LoadNextLevel(float timeDelay)
    {
        yield return new WaitForSeconds(timeDelay);

        //SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        curLevel++;
        if (curLevel > totalLevels)
            curLevel = 1;
        string str = "Level" + curLevel.ToString();

        SceneManager.LoadScene(str);
        PlayerPrefs.SetInt("curLevel", curLevel);
        //Debug.Log("str " + str);
    }

}
