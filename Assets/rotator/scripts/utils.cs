﻿using UnityEngine;
using System.Collections;

public class utils : MonoBehaviour {

    public static void FitColliderToChildren(GameObject parentObject)
    {
        BoxCollider bc = parentObject.GetComponent<BoxCollider>();
        if (bc == null)
        {
            bc = parentObject.AddComponent<BoxCollider>();
        }
        Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);
        bool hasBounds = false;
        Renderer[] renderers = parentObject.GetComponentsInChildren<Renderer>();
        foreach (Renderer render in renderers)
        {
            if (hasBounds)
            {
                bounds.Encapsulate(render.bounds);
            }
            else
            {
                bounds = render.bounds;
                hasBounds = true;
            }
        }
        if (hasBounds)
        {
            bc.center = bounds.center - parentObject.transform.position;
            bc.size = bounds.size;
        }
        else
        {
            bc.size = bc.center = Vector3.zero;
            bc.size = Vector3.zero;
        }
    }

    public static void FitColliderToChildren(GameObject parentObject, Bounds bounds)
    {
        BoxCollider bc = parentObject.GetComponent<BoxCollider>();
        if (bc == null)
        {
            bc = parentObject.AddComponent<BoxCollider>();
        }
        //Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);
        bool hasBounds = false;
        Renderer[] renderers = parentObject.GetComponentsInChildren<Renderer>();
        foreach (Renderer render in renderers)
        {
            if (hasBounds)
            {
                bounds.Encapsulate(render.bounds);
            }
            else
            {
                bounds = render.bounds;
                hasBounds = true;
            }
        }
        if (hasBounds)
        {
            bc.center = bounds.center - parentObject.transform.position;
            bc.size = bounds.size;
        }
        else
        {
            bc.size = bc.center = Vector3.zero;
            bc.size = Vector3.zero;
        }
    }


    public static void FitColliderToBounds(GameObject parentObject, Bounds _bounds)
    {
        BoxCollider bc = parentObject.GetComponent<BoxCollider>();
        Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);
        bounds.Encapsulate(_bounds);
        bc.center = bounds.center ;
        Vector3 size = bounds.size;
        size.z = size.z+2;
        size.x = size.x+2;
        bc.size = size;
    }

}
