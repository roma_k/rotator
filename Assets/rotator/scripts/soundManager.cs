﻿using UnityEngine;
using System.Collections;

public class soundManager : MonoBehaviour
{

    public AudioSource audioSource;
    public AudioSource playerAudioSource;
    public AudioSource musicAudioSource;

    void Start()
    {

    }

    void Update()
    {

    }

    public void PlaySound(AudioClip audioClip, float volume = 0f)
    {
        if (audioSource != null && audioSource != null)
        {
            audioSource.Stop();
        }

        if (volume != 0)
            audioSource.volume = volume;
        audioSource.PlayOneShot(audioClip);
    }

    public void PlayPlayerSound(AudioClip audioClip, float volume = 0f)
    {
        if (playerAudioSource != null && playerAudioSource != null)
        {
            audioSource.Stop();
        }

        if (volume != 0)
            playerAudioSource.volume = volume;
        playerAudioSource.PlayOneShot(audioClip);
    }

    public void PlayMusic(AudioClip a, bool isLooping, float volume = 0f)
    {
        if (musicAudioSource != null && musicAudioSource.clip != null)
        {
            musicAudioSource.Stop();
        }

        musicAudioSource.clip = a;
        musicAudioSource.loop = isLooping;
        if (volume != 0)
            musicAudioSource.volume = volume;
        musicAudioSource.Play();
    }

}
