﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class rotator : MonoBehaviour
{

    public float speedX = 0;
    public float speedY = 0;
    public float speedZ = 0;

    float angle;

    void Start()
    {
    }

    void Update()
    {
        transform.Rotate(new Vector3(speedX, speedY, speedZ) * Time.deltaTime);
    }
}
