﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class player : MonoBehaviour
{

    public bool showEffectOnStart;
    public GameObject startEffectPrefab;
    public float effectWait;
    public GameObject levelStart;
    public GameObject levelEnd;

    private bool isActive;
    private Rigidbody rb;

    private bool magnetAcive;
    private float magnetLifeTime = 0f;
    private float magnetRadius = 0f;
    private float magnetTimer = 0f;
    private float magnetMovePickupSpeed = 7f;
    private GameObject[] pickups;

    public Text scoreText;
    int score;
    int totalPickups;

    void Awake()
    {
        totalPickups = 0;
        levelStart = GameObject.FindGameObjectWithTag("LevelStart");
        levelEnd = GameObject.FindGameObjectWithTag("LevelEnd");
        
        gameObject.transform.position = levelStart.transform.position;

        rb = gameObject.GetComponent<Rigidbody>();
        isActive = true;
        if (showEffectOnStart)
        {
            gameObject.GetComponent<Renderer>().enabled = false;
            rb.useGravity = false;
            isActive = false;
        }

        pickups = GameObject.FindGameObjectsWithTag("PickUp");
        totalPickups = pickups.Length;
    }

    IEnumerator Start()
    {
        ResetScore();

        if (showEffectOnStart)
        {

            GameObject ob = Instantiate(startEffectPrefab, transform.position, Quaternion.identity) as GameObject;
            yield return new WaitForSeconds(effectWait);
            Destroy(ob);
            gameObject.GetComponent<Renderer>().enabled = true;
            rb.useGravity = true;
            isActive = true;
        }
    }

    public void ResetScore()
    {
        score = 0;
        scoreText.text = "" + score.ToString() + " of " + totalPickups.ToString();
    }

    public void AddScore(int num)
    {
        score += num;
        scoreText.text = "" + score.ToString() + " of " + totalPickups.ToString();
    }

    public bool IsActive()
    {
        return isActive;
    }

    void Update()
    {
        ProcessMagnet();
    }

    void OnCollisionExit(Collision col)
    {

    }

    IEnumerator DoRestartLevel()
    {
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("LevelEnd"))
        {
            Debug.Log("WIN !");
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            rb.velocity = Vector3.zero;
            rb.isKinematic = true;
            gameObject.GetComponent<Renderer>().enabled = false;

            StartCoroutine(gameManager.instance.LoadNextLevel(1f));
            //StartCoroutine(DoRestartLevel());
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("LevelBounds"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }


    // === MAGNET ===
    public bool MagnetActive()
    {
        return magnetAcive;
    }

    public void ActivateMagnet(float lifeTime, float radius)
    {
        magnetAcive = true;
        magnetLifeTime = lifeTime;
        magnetRadius = radius;
    }

    IEnumerator GetPickupAnim(GameObject pickup)
    {
        float step = magnetMovePickupSpeed * Time.deltaTime;
        pickup.transform.position = Vector3.MoveTowards(pickup.transform.position, transform.position, step);

        yield return null;
    }

    void ProcessMagnet()
    {
        if (!magnetAcive)
            return;

        magnetTimer += Time.deltaTime;
        if (magnetTimer >= magnetLifeTime)
        {
            magnetTimer = 0f;
            magnetAcive = false;
        }

        foreach (GameObject pickup in pickups)
        {
            if (!pickup.activeSelf)
                continue;

            float distance = Vector3.Distance(transform.position, pickup.transform.position);

            if (distance <= magnetRadius)
            {
                StartCoroutine(GetPickupAnim(pickup));
            }

        }
    }
    // === MAGNET ===

}
